package pkg

import "fmt"

type wallet struct {
	balance int
}

func newWallet() *wallet {
	return &wallet{
		balance: 0,
	}
}

func (w *wallet) creaditBalance(amount int) {
	w.balance += amount
	fmt.Println("Wallet balance added successfully")
}

func (w *wallet) debitBalance(amount int) error{
	if w.balance < amount {
		return fmt.Errorf("balance is not sufficient")
	}

	fmt.Println("Wallet balance is Sufficient")
	w.balance -= amount
	return nil
}