package pkg

import "fmt"

type secureCode struct {
	code int
}

func newSecureCode(code int) *secureCode {
	return &secureCode{
		code: code,
	}
}

func (s *secureCode) checkSecureCode(code int) error {
	if s.code != code {
		return fmt.Errorf("code is incorrect")
	}
	fmt.Println("Code is verified")
	return nil
}