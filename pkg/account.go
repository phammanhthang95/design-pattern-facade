package pkg

import "fmt"

type account struct {
	name string
}

func newAccount(name string) *account {
	return &account{
		name: name,
	}
}

func (s *account) checkAccount(name string) error {
	if s.name != name {
		return fmt.Errorf("account Name is incorrect")
	}
	fmt.Println("Account is verified")
	return nil
}