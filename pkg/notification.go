package pkg

import "fmt"

type notification struct {

}

func (n *notification) sendWalletCreditNofitication() {
	fmt.Println("Sending wallet credit notification")
}

func (n *notification) sendWalletDebitNofitication() {
	fmt.Println("Sending wallet debit notification")
}

