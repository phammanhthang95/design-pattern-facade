package pkg

import "fmt"

type WalletFacade struct {
	account *account
	wallet *wallet
	secureCode *secureCode
	notification *notification
}

func NewWalletFacade(accountId string, code int) *WalletFacade {
	fmt.Println("Starting create account")
	WalletFacade := &WalletFacade{
		account: newAccount(accountId),
		secureCode: newSecureCode(code),
		wallet: newWallet(),
		notification: &notification{},
	}
	return WalletFacade
}

func (w *WalletFacade) AddMoneyToWallet(accountID string, securityCode int, amount int) error{
	fmt.Println("Starting add money to wallet")
	err := w.account.checkAccount(accountID)
	if err != nil {
		return err
	}
	err = w.secureCode.checkSecureCode(securityCode)
	if err != nil {
		return err
	}
	w.wallet.creaditBalance(amount)
	w.notification.sendWalletCreditNofitication()
	return nil
}

func (w *WalletFacade) DebutMoneyToWallet(accountID string, securityCode int, amount int) error{
	fmt.Println("Starting debut money to wallet")
	err := w.account.checkAccount(accountID)
	if err != nil {
		return err
	}
	err = w.secureCode.checkSecureCode(securityCode)
	if err != nil {
		return err
	}
	err =  w.wallet.debitBalance(amount)
	if err != nil {
		return err
	}
	w.notification.sendWalletCreditNofitication()
	return nil
}