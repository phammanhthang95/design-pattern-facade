package main

import (
	"facade/pkg"
	"log"
)

func main() {
	wallFacade := pkg.NewWalletFacade("abc", 1234)

	err := wallFacade.AddMoneyToWallet("abc", 1234, 10)
	if err != nil {
		log.Fatalf("Error: %s\n", err.Error())
	}

	err = wallFacade.DebutMoneyToWallet("abc", 1234, 5)
	if err != nil {
		log.Fatalf("Error: %s\n", err.Error())
	}
}